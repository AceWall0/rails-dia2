Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/contato', to: 'contact#form', as: :contact_form
  post '/destino', to: 'contact#destine', as: :destiny
end
